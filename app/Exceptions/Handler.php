<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, \Throwable $e)
    {
        return $this->handleApiException($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse|RedirectResponse|Response
     */
    protected function unauthenticated(
        $request,
        AuthenticationException $exception
    ) {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }

    /**
     * Override ValidationException invalidJson()
     *
     * @param Request $request
     * @param ValidationException $exception
     * @return Response|JsonResponse
     */
    protected function convertValidationExceptionToResponse(
        $request,
        $exception
    ) {
        $message = [];
        foreach ($exception->errors() as $key => $value) {
            $message[] = [
                'field' => $key,
                'message' => $value[0],
            ];
        }

        return $request->expectsJson()
            ? response_api('400', 'error', 'Validation Exception', $message)
            : $this->invalid($request, $exception);
    }

    private function handleApiException($request, $exception)
    {
        $prepareException = $this->prepareException($exception);

        if ($prepareException instanceof AuthenticationException) {
            return $this->unauthenticated($request, $prepareException);
        }

        if ($prepareException instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse(
                $request,
                $prepareException
            );
        }

        if ($exception instanceof TokenMismatchException) {
            return redirect($request->fullUrl())->with(
                'csrf_error',
                "Oops! Seems you couldn't submit form for a long time. Please try again."
            );
        }

        if ($exception instanceof AuthorizationException) {
            if (!$request->expectsJson()) {
                return abort(403, 'Unauthorized action.');
            }
        }

        return $request->expectsJson()
            ? $this->customApiResponse($prepareException)
            : $this->prepareResponse($request, $exception);
    }

    private function customApiResponse($exception)
    {
        $statusCode = 500;
        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
        }

        switch ($statusCode) {
            case 401:
                $message = 'Unauthorized';
                break;
            case 403:
                $message = 'Forbidden';
                break;
            case 404:
                $message = 'Not Found';
                break;
            case 405:
                $message = 'Method Not Allowed';
                break;
            default:
                $message =
                    $statusCode == 500
                        ? 'Whoops, looks like something went wrong'
                        : $exception->getMessage();
                break;
        }

        if (config('app.debug')) {
            $response['code'] = $exception->getCode();
            $response['file'] = $exception->getFile();
            $response['line'] = $exception->getLine();
            $response['message'] = $exception->getMessage();
            $response['trace'] = $exception->getTrace();
        }

        return response_api($statusCode, false, $message, $response ?? []);
    }
}
