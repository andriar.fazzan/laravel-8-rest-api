<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Http\Requests\UserStoreRequest;

class UserController extends Controller
{
    public function store(UserStoreRequest $request)
    {
        $data = User::create($request->validated());

        return response_api(
            Response::HTTP_CREATED,
            true,
            __('user.created'),
            $data
        );
    }

    public function update(UserStoreRequest $request, $id)
    {
        $data = User::findOrFail($id);

        return response_api(Response::HTTP_OK, true, __('user.updated'), $data);
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);

        $data->delete();

        return response_api(Response::HTTP_OK, true, __('user.deleted'));
    }
}
